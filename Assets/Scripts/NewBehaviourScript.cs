﻿using UnityEngine;
using MyStore;

public class NewBehaviourScript : MonoBehaviour
{ 
    static Console[] consoles;
    static Game[] games;

    static float total;
    // Start is called before the first frame update
    void Start()
    {
        consoles = new Console[3];
        consoles[0] = new PlayStation("PlayStation 4", PlayStation.Type.PlayStation_4, 1500);
        consoles[1] = new Nintendo("Switch", Nintendo.Type.Switch, 900);
        consoles[2] = new Xbox("XBox 360 CrysisEdition", Xbox.Type.Xbox_360, 1600);

        games = new Game[2];
        games[0] = new Digital("Crysis 3", Digital.Platform.Steam, 215);
        games[1] = new Physical("COD MW", 245);


        Loop();
    }   

    void Loop()
    {
        Debug.Log("Consoles");
        Debug.Log("========");

            for (int i = 0; i < consoles.Length; i++)
            {
                Debug.Log("\t" + (1 + i) + " - " + consoles[i].name + " S/. " + consoles[i].price);
            }

            Debug.Log("Games");
            Debug.Log("========");

            for (int i = 0; i < games.Length; i++)
            {
                Debug.Log("\t" + (4 + i) + " - " + games[i].name + " S/. " + games[i].price);
            }

            string str = "4";
            int option = int.Parse(str);
            if (option < 4)
            {
                total += consoles[option - 1].price;
            }
            else if (option < 6)
            {
                total += games[option - 4].price;
            }
            else
            {
                Debug.Log("total" + total);
            }
    }
}

