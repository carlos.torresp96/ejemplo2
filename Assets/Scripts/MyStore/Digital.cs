﻿using MyStore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyStore
{
    class Digital : Game
    {
        public enum Platform
        {
            PlayStation_Store,
            EShop,
            Steam
        }

        public Platform platform;

        public Digital (string name, Platform platform, float price) : base(name, price)
        {
            this.platform = platform;
        }
    }
}
