﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyStore
{
    public class PlayStation : Console
    {
        public enum Type
        {
            PlayStation_3,
            PlayStation_4,
            PlayStation_4_pro
        }

        public Type type;
        public PlayStation(string name, Type type, float price) : base(name, price)
        {
            this.type = type;
            
        }

        public void Connect_Controller()
        {

        }
    }
}
