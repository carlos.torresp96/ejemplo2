﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyStore
{
   class Game
    {
        public string name;
        public float price;

        public Game(string name, float price)
        {
            this.name = name;
            this.price = price;
        }
    }
}
