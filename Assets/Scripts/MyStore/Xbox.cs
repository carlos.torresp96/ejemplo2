﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyStore
{
    public class Xbox : Console
    {
        public enum Type
        {
            Xbox_One,
            Xbox_360,
        }

        public Type type;
        public Xbox(string name, Type type, float price) : base(name, price)
        {
            this.type = type;
        }
        public void ConnectKinetic()
        {

        }
    }
}
