﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyStore
{
    class Nintendo : Console
    {
        public enum Type
        {
         Wii_U,
         Nintendo_3DS,
         Switch
        }

        public Type type;
        public Nintendo(string name,Type type, float price) : base(name, price)
        {
            this.type = type;
        }
        
        public void InsertCartridge()
        {

        }
    }
}
