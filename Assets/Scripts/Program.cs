﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyStore;

namespace ConsoleApp3
{
    class Program
    {
        static String next;
        static MyStore.Console[] consoles;
        static Game[] games;

        static float total;

        static void Main(string[] args)
        {
            bool loop = true;
            consoles = new MyStore.Console[3];
            consoles[0] = new PlayStation("PlayStation 4", PlayStation.Type.PlayStation_4, 1500);
            consoles[1] = new Nintendo("Switch", Nintendo.Type.Switch, 900);
            consoles[2] = new Xbox("XBox 360 CrysisEdition", Xbox.Type.Xbox_360, 1600);

            games = new Game[2];
            games[0] = new Digital("Crysis 3", Digital.Platform.Steam, 215);
            games[1] = new Physical("COD MW", 245);

            while(loop)
            {
                System.Console.Clear();

                Loop();

                System.Console.WriteLine("Continue? (Y/N)");
                next = System.Console.ReadLine();
                loop = (next.ToUpper() == "Y");
            }
             void Loop()
            {
                System.Console.WriteLine("Consoles");
                System.Console.WriteLine("========");

                for (int i=0; i< consoles.Length; i++)
                {
                    System.Console.WriteLine("\t"+(1+i)+" - " + consoles[i].name + " S/. " + consoles[i].price);
                }

                System.Console.WriteLine("Games");
                System.Console.WriteLine("========");

                for (int i=0; i < games.Length; i++)
                {
                    System.Console.WriteLine("\t" + (4 + i) + " - " + games[i].name + " S/. " + games[i].price);
                }

                string str = System.Console.ReadLine();
                int option = Int32.Parse(str);
                if (option < 4)
                {
                    total += consoles[option - 1].price;                   
                }
                else if (option < 6)
                {
                    total += games[option - 4].price;
                }
                else
                {
                    System.Console.WriteLine("total" + total);
                }
            }
        }
        
    }
}
